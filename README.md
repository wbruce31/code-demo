# Demo RESTful service using Spring Boot

## Clone Project
`git clone git@gitlab.com:wbruce31/code-demo.git`

## Running the Service

### Build and Run (Maven Required)
1. Clone the project
2. Run the following command from the root: `mvn package && java -jar target/demo-0.0.1-SNAPSHOT.jar`

### Run Jar
1. CD into src/test/resources/
2. Run the following command: `java -jar demo-0.0.1-SNAPSHOT.jar`

## Using the Service

### GET Methods
**http://localhost:8080/shelter/**
* /dogs
* /dogs/{id}
* /dogs/{id}/name
* /dogs/{id}/breed

### DELETE Method
`curl -XDELETE 'http://localhost:8080/shelter/dogs/{id}'`

### PUT Method
`curl -XPUT -H "Content-type: application/json" -d '{"name":"Reggie","breed":"Black Lab"}' 'http://localhost:8080/shelter/dogs/'`