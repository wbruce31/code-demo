package dev.wade.demo.delegates;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import dev.wade.demo.model.Animal;
import dev.wade.demo.model.Dog;

@Component
public class DogDelegate {

	private Map<Long, Animal> dogs;
	private long idSequence = 0;
	
	public DogDelegate() {
		dogs = new HashMap<>();
		addDog(new Dog("Finley", "Beagle"));
		addDog(new Dog("Lacy", "Papillon"));
	}
	
	public Collection<Animal> getDogs() {
		return dogs.values();
	}
	
	public Animal getPet(long id) {
		return dogs.get(id);
	}
	
	public long addDog(Dog dog) {
		long id = getNextId();
		dog.setId(id);
		dogs.put(id, dog);
		return id;
	}
	
	public void removeDog(long id) {
		dogs.remove(id);
	}
	
	private long getNextId() {
		return idSequence++;
	}
	
}
