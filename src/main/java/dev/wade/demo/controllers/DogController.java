package dev.wade.demo.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dev.wade.demo.delegates.DogDelegate;
import dev.wade.demo.model.Animal;
import dev.wade.demo.model.Dog;

@RestController
@RequestMapping(path="/shelter")
public class DogController {

	@Autowired
	DogDelegate dogDelegate;
	
	@RequestMapping(path = "/dogs", method = RequestMethod.GET)
    public Collection<Animal> getAllDogs() {
    	return dogDelegate.getDogs();
    }
    
	@RequestMapping(path = "/dogs/{id}", method = RequestMethod.GET)
    public Animal getDogById(@PathVariable("id") long id) {
    	return dogDelegate.getPet(id);
    }

    @RequestMapping(path = "/dogs/{id}/name", method = RequestMethod.GET)
    public String getDogsName(@PathVariable("id") long id) {
    	Animal dog = getDogById(id);
    	return dog.getName();
    }
    
    @RequestMapping(path = "/dogs/{id}/breed", method = RequestMethod.GET)
    public String getDogsBreed(@PathVariable("id") long id) {
    	Animal dog = getDogById(id);
    	return dog.getBreed();
    }
    
    @RequestMapping(path = "/dogs", method = RequestMethod.PUT)
    public long addDog(@RequestBody Dog dog) {
    	return dogDelegate.addDog(dog);
    }
    
	@RequestMapping(path = "/dogs/{id}", method = RequestMethod.DELETE)
    public void removeDogById(@PathVariable("id") long id) {
    	dogDelegate.removeDog(id);
    }
}
