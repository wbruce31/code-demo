package dev.wade.demo.model;

public class Dog implements Animal {

	private String name;
	private String breed;
	private long id;

	public Dog() {
	}
	
	public Dog(String name, String breed) {
		super();
		this.name = name;
		this.breed = breed;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getBreed() {
		return breed;
	}

	@Override
	public long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public void setId(long id) {
		this.id = id;
	}

}
