package dev.wade.demo.model;

public interface Animal {

	public String getName();
	public String getBreed();
	public long getId();
	
}
