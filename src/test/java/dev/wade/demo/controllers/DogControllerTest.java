package dev.wade.demo.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import dev.wade.demo.app.DemoApp;
import dev.wade.demo.delegates.DogDelegate;
import dev.wade.demo.model.Dog;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DemoApp.class })
@WebAppConfiguration
public class DogControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Mock
	DogDelegate delegate;

	@Autowired
	@InjectMocks
	private DogController controller;

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getDogByIdRetrievesFromDB() throws Exception {
		long id = 1;
		Dog dog = new Dog("Finley", "Beagle");
		dog.setId(id);
		when(delegate.getPet(id)).thenReturn(dog);
		MvcResult result = mockMvc.perform(get("/shelter/dogs/" + id)).andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		Assert.assertTrue(content.contains("Finley"));
		Assert.assertTrue(content.contains("Beagle"));
		Assert.assertTrue(content.contains(Long.toString(id)));
		verify(delegate, times(1)).getPet(id);
	}
}
